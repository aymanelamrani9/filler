/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_extractpiece.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aelamran <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/31 16:39:52 by aelamran          #+#    #+#             */
/*   Updated: 2019/11/01 16:11:20 by aelamran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

char	**ft_extractpiece(void)
{
	char	**piece;
	char	*line;
	int		size;
	int		i;

	i = 0;
	size = -1;
	line = NULL;
	while (get_next_line(0, &line))
	{
		if (ft_strstr(line, "Piece"))
		{
			while (!ft_isdigit(*line))
				line++;
			size = ft_atoi(line);
			piece = (char **)malloc(sizeof(char *) * (size + 1));
		}
		if (size != -1 && (ft_strchr(line, '.') || ft_strchr(line, '*')))
			piece[i++] = ft_strdup(line);
		if (size == i)
			break ;
	}
	piece[i] = NULL;
	return (piece);
}
