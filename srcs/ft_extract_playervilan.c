/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_extract_playervilan.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aelamran <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/31 16:41:24 by aelamran          #+#    #+#             */
/*   Updated: 2019/10/31 16:42:13 by aelamran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

void	ft_extract_playervilan(t_infos *infos)
{
	char	*line;

	line = NULL;
	while (get_next_line(0, &line))
	{
		if (ft_strstr(line, "exec"))
		{
			if (ft_strchr(line, '1'))
			{
				infos->player = 'O';
				infos->vilan = 'X';
			}
			else
			{
				infos->player = 'X';
				infos->vilan = 'O';
			}
			break ;
		}
	}
}
