/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aelamran <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/19 16:39:59 by aelamran          #+#    #+#             */
/*   Updated: 2019/11/03 20:04:50 by aelamran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

void	ft_del(void	*content, size_t content_size)
{
	if (content)
		free(content);
}

void	ft_freeinfos(t_infos *infos)
{
	ft_freetab(infos->map);
	ft_freetab(infos->piece);
	ft_lstdel(&infos->lst_cords, ft_del);
}

int		ft_launch_algo(t_infos *infos, char **old_map)
{
	t_list	*lst;
	t_cord	*vcord;

	if (!(vcord = ft_ennemi_new_piece_pos(infos->map, old_map, infos->vilan)))
		vcord = ft_newcords(0, 0);
	if ((lst = ft_retrievecords(infos->map, infos->lst_cords, infos->player)))
	{
		ft_makedecision(lst, vcord);
		ft_lstdel(&lst, ft_del);
		free(vcord);
	}
	else
	{
		write(1, "0 0\n", 4);
		return (0);
	}
	return (1);
}

int		ft_launch_algo_up(t_infos *infos)
{
	t_list	*lst;
	t_cord	*vcord;

	vcord = ft_newcords(0, ft_getypos(infos->map));
	if ((lst = ft_retrievecords(infos->map, infos->lst_cords, infos->player)))
	{
		ft_makedecision(lst, vcord);
		ft_lstdel(&lst, ft_del);
		free(vcord);
	}
	else
	{
		write(1, "0 0\n", 4);
		return (0);
	}
	return (1);
}

int		ft_launch(t_infos *infos, char **old_map)
{
	if (infos->player == 'X' && ft_checkwhere_x(infos->map))
	{
		if (!ft_launch_algo_up(infos))
		{
			free(infos);
			return (0);
		}
	}
	else if (!ft_launch_algo(infos, old_map))
	{
		free(infos);
		return (0);
	}
	return (1);
}

int		main(void)
{
	char	**old_map;
	t_infos	*infos;

	if (!(infos = (t_infos *)malloc(sizeof(t_infos))))
		return (0);
	ft_extract_playervilan(infos);
	old_map = NULL;
	while (1)
	{
		infos->map = ft_extractmap();
		infos->piece = ft_extractpiece();
		infos->lst_cords = ft_extractcords(infos->piece);
		if (!ft_launch(infos, old_map))
			return (0);
		ft_freetab(old_map);
		old_map = ft_clonemap(infos->map);
		ft_freeinfos(infos);
	}
	free(infos);
	return (0);
}
