/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_checkwhere_x.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aelamran <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/03 19:49:00 by aelamran          #+#    #+#             */
/*   Updated: 2019/11/03 19:49:22 by aelamran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

int		ft_checkwhere_x(char **map)
{
	int i;

	i = 0;
	while (map[i])
	{
		if (ft_strchr(map[i], 'X'))
		{
			if (i >= 2)
				return (1);
			else
				return (0);
		}
		i++;
	}
	return (-1);
}
