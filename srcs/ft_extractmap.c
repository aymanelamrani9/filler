/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_extractmap.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aelamran <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/31 16:39:03 by aelamran          #+#    #+#             */
/*   Updated: 2019/11/02 13:57:36 by aelamran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

char	**ft_extractmap(void)
{
	char	**map;
	char	*line;
	int		size;
	int		i;

	i = 0;
	size = -1;
	line = NULL;
	map = NULL;
	while (size != i && get_next_line(0, &line) > 0)
	{
		if (ft_strstr(line, "Plateau"))
		{
			while (!ft_isdigit(*line))
				line++;
			size = ft_atoi(line);
			map = (char **)malloc(sizeof(char *) * (size + 1));
		}
		if (size != -1 && (ft_strchr(line, '.') ||
					ft_strchr(line, 'O') || ft_strchr(line, 'X')))
			map[i++] = ft_strdup(line + 4);
	}
	map[i] = NULL;
	return (map);
}
