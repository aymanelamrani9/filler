/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_getypos.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aelamran <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/03 19:47:33 by aelamran          #+#    #+#             */
/*   Updated: 2019/11/03 19:52:13 by aelamran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

int		ft_getypos(char **map)
{
	int i;
	int j;
	int ypos;

	i = 0;
	while (map[i])
	{
		j = 0;
		if (ft_strchr(map[i], 'X'))
		{
			while (map[i][j])
			{
				if (map[i][j] == 'X')
					ypos = j;
				j++;
			}
			return (ypos);
		}
		i++;
	}
	return (-1);
}
